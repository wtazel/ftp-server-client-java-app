package ftpserver;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class FTPServer extends Thread {
	   private ServerSocket serverSocket;
	   public static Map<String, String> map;
	   File f; File[] lf;
	   
	   public FTPServer(int port) throws IOException {
	      serverSocket = new ServerSocket(port);
	      serverSocket.setSoTimeout(60000);
	   }
	   
	   public static void InitDbUtilizatori() {
		   map = new HashMap<String, String>();
		   map.put("admin", "1234");
		   map.put("utilizator", "parola");
	   }
	   

	   public void run() {
	      while(true) {
	         try {
	            Socket server = serverSocket.accept();
	            DataInputStream in = new DataInputStream(server.getInputStream());
	            DataOutputStream out = new DataOutputStream(server.getOutputStream());
	            boolean aut_succes = false;
	            
	            InitDbUtilizatori();

	            //Autentifica clientul
	            out.writeUTF("332");
	            String aut = in.readUTF();
	            if(map.containsKey(aut)) {
	            	String usr = aut;
	            	out.writeUTF("331");
	            	aut = in.readUTF();
	            	if(map.get(usr).equals(aut)) {
	            		out.writeUTF("200");
	            		aut_succes = true;
	            	} else {
	            		out.writeUTF("430");
	            		server.close();
	            	}
	            } else {
	            	out.writeUTF("430");
	            	server.close();
	            }
	            
	            if(aut_succes) {
	            	System.out.println(server.getRemoteSocketAddress() + " s-a conectat si s-a autentificat.");
		            String command = "";
		            f = new File("D:\\");
		            lf = f.listFiles();
		            
		            while((command = in.readUTF()) != null) {
			            if(command.trim().startsWith("download")) {
			            	if(command.trim().equals("download")) {
			            		//Comanda Incompleta
			            		out.writeUTF("501");
			            	} else {
			            		String[] cmd = command.split("\\s+");
			            		if(cmd[0].equals("download")) {
			            			File aux = new File(f.getAbsolutePath() + "\\" + cmd[1]);
			            			if(aux.exists() && aux.isFile()) {
			            
			            				//Deschide fisierul si trimite-l
			            				out.writeUTF(String.valueOf((int)aux.length()) + "*" + aux.getName()); //Trimite lungimea si numele fisierului
			            				
			            		        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(aux));
			            		        if(aux.length() <= 10000) {
			            		        	byte [] c = new byte [(int)aux.length()+1];
			            		        	bis.read(c, 0 ,c.length);
			            		        	out.write(c, 0, c.length);
			            		        }
			            		        bis.close();
			            			} else {
			            				out.writeUTF("501");
			            			}
			            		} else {
			            			out.writeUTF("502");
			            		}
			            	}
			            	
			            } else if(command.equals("quit")) {
			            	//Clientul inchide conexiunea
			            	System.out.println(server.getRemoteSocketAddress() + " s-a deconectat");
			            	server.close();
			            	break;
			            } else if(command.trim().startsWith("rename")) {
			            	//Redenumire fisiere
			            	if(command.trim().equals("rename")) {
			            		out.writeUTF("501");
			            	} else {
			            		String[] cmd = command.split("\\s+", 2);
			            		if(cmd[0].equals("rename")) {
			            			File aux = new File(f.getAbsolutePath() + "\\" + cmd[1]);
			            				if (aux.exists()) {
			            					aux.renameTo(new File(f.getAbsolutePath() + "\\" + cmd[2]));
			            					lf = f.listFiles();
			            					out.writeUTF("200");
			            				} else {
				            				out.writeUTF("501");
				            			}
			            			
			            		} else {
		            				out.writeUTF("502");
		            			}
			            	}
			            } else if(command.trim().startsWith("rm")) {
			            	//Stergere fisiere
			            	if(command.trim().equals("rm")) {
			            		out.writeUTF("501");
			            	} else {
			            		String[] cmd = command.split("\\s+", 2);
			            		if(cmd[0].equals("rm")) {
			            			File aux = new File(f.getAbsolutePath() + "\\" + cmd[1]);
			            				if (aux.exists()) {
			            					aux.delete();
			            					lf = f.listFiles();
			            					out.writeUTF("200");
			            				} else {
				            				out.writeUTF("501");
				            			}
			            			
			            		} else {
		            				out.writeUTF("502");
		            			}
			            	}
			            } else if(command.equals("ls")) {
			            	//Afisare lista fisiere si directoare
			            	String res = "";
			            	for(int i=0;i<lf.length;i++) {
			            		if(lf[i].isFile()) {
			            			res += lf[i].getName() + "  --  " + lf[i].length() +" Bytes  --  File\n";
			            		} else if(lf[i].isDirectory()) {
			            			res += lf[i].getName() + "  --  Folder\n";
			            		}
			            	}
			            	out.writeUTF(res);
			            } else if(command.trim().startsWith("cd")) {
			            	//Schimbare director
			            
			            	if(command.trim().equals("cd")) {
			            		out.writeUTF(f.getPath());
			            	} else {
			            		String[] cmd = command.split("\\s+", 2);
			            		if(cmd[0].equals("cd")) {
			            			if(cmd[1].equals("..")) {
			            				if(f.getParent() != null) {
			            					f = new File(f.getParent());
				            				lf = f.listFiles();
				            				out.writeUTF(f.getAbsolutePath());
			            				} else {
			            					out.writeUTF(f.getAbsolutePath());
			            				}
			            			} else {
				            			File aux = new File(f.getAbsolutePath() + "\\" +cmd[1]);
				            			
				            			if(aux.exists() && aux.isDirectory()) {
				            				f = aux;
				            				lf = f.listFiles();
				            				out.writeUTF(aux.getPath());
				            			} else {
				            				out.writeUTF("501");
				            			}
			            			}
			            		} else {
		            				out.writeUTF("502");
		            			}
			            	}
			            } else {
			            	//Comanda primita nu exista
			            	out.writeUTF("502");
			            }
		            }
		            
	            } //ENDIF aut_succes
	            
	            
	            /*DataOutputStream out = new DataOutputStream(server.getOutputStream());
	            out.writeUTF("Thank you for connecting to " + server.getLocalSocketAddress()
	               + "\nGoodbye!");
	            server.close();*/
	            
	         }catch(SocketTimeoutException s) {
	            System.out.println("Timeout!");
	            break;
	         } catch(SocketException e) {
	        	 System.out.println("Clientul s-a deconectat");
	         } catch(IOException e) {
	            e.printStackTrace();
	            break;
	         }
	      }
	   }
	   
	   public static void main(String [] args) {
	      int port = 21;
	      try {
	         Thread t = new FTPServer(port);
	         t.start();
	      }catch(IOException e) {
	         e.printStackTrace();
	      }
	   }
}
