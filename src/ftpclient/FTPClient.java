package ftpclient;

import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.Scanner;

/* TODO: Verifica constant daca serverul mai este online */

public class FTPClient {
	public final static String loc_download = "C:\\Users\\Andy\\Downloads\\";
	public static Socket client;
	public static DataOutputStream out;
	public static DataInputStream in;
	public static Scanner sc;
	public static boolean FTPAutentificare() throws IOException {
		String ret = in.readUTF();
		sc = new Scanner(System.in);
	       if(ret.equals("332")) {
	    	   
	    	   System.out.print("Username: ");
	    	   String resp = sc.next();
	    	   out.writeUTF(resp);
	    	   if(in.readUTF().equals("331")) {
	    		   System.out.print("Password: ");
	    		   resp = sc.next();
	    		   out.writeUTF(resp);
	    		   if(in.readUTF().equals("200")) {
	    			   
	    			   return true;
	    		   } else {
	    			   System.out.println("Parola gresita!");
	    			   client.close();
	    		   }
	    	   } else {
	    		   System.out.println("User-ul nu exista!");
	    		   client.close();
	    	   }

	       }
	       
	       return false;
	}
	
	public static void main(String [] args) {
		String serverName = "localhost";
	    int port = 21;
	    try {
	       System.out.println("Se conecteaza(" + serverName + ":" + port + ")...");
	       client = new Socket(serverName, port);
	       out = new DataOutputStream(client.getOutputStream());
	       in = new DataInputStream(client.getInputStream());
	       boolean autentificat;
	       
	       autentificat = FTPAutentificare();
	       
	       if(autentificat == true) {
	    	   System.out.println("Conectat la " + client.getRemoteSocketAddress());
	    	   System.out.println();
	    	   
	    	   String comanda = "";
	    	   String dir_curent = "\\";
	    	   sc.nextLine();
	    	   while(!comanda.equals("quit")) {
	    		   
	    		   System.out.print(client.getRemoteSocketAddress() + dir_curent + ">");
	    		   	
	    		   comanda = sc.nextLine();
	    		   if(comanda.isEmpty()) continue;
	    		   out.writeUTF(comanda);
	    		   if(comanda.equals("quit")) break;
	    		   String resp = in.readUTF();
	    		   if(resp.equals("502")) {
	    			   System.out.println("Comanda nu exista");
	    		   } else if(resp.equals("501")) {
	    			   System.out.println("Comanda incorecta");
	    		   } else if(comanda.equals("ls")) {
	    			   System.out.println(resp);
	    		   } else if(comanda.startsWith("cd")) {
	    			   dir_curent = resp.replaceAll("^(D:)", "");
	    		   } else if(comanda.startsWith("download")) {
	    			   int l_fisier; String nume_fisier;
	    			   InputStream is = null; BufferedOutputStream bos = null; 
	    		    	
	    			   String[] info_fisier;
	    			   info_fisier = resp.split("\\*");
	    			   l_fisier = Integer.parseInt(info_fisier[0]);
	    			   nume_fisier = info_fisier[1];
	    			   try {
	    				   if(l_fisier > 10000) {
			    		    	System.out.println("Nu se pot descarca fisiere mai mari de 10000 Bytes");
			    		   } else {
			    		    	bos = new BufferedOutputStream(new FileOutputStream(loc_download + nume_fisier));
			    		    	byte [] c  = new byte [l_fisier+1];
			    		    	in.read(c, 0, c.length);
				    		    bos.write(c, 0 , c.length);
				    		    bos.flush();
				    		    System.out.println("Fisierul " + nume_fisier + " a fost descarcat in " + loc_download + nume_fisier);
			    		    }

	    		    	} finally {
		    		    	if(bos!=null) bos.close(); 
		    		    	if(is!=null) is.close();
		    		    }

	    		    }
	    	   }
		       sc.close();
		       System.out.println("Deconectat de la server");
	       }
	       client.close();
	    } catch (ConnectException e) {
	    	System.out.println("Nu s-a putut conecta");
	    } catch (SocketException e) {
	    	System.out.println("Conexiunea s-a inchis");
	    }
	    catch(IOException e) {
	       e.printStackTrace();
	    } 
	}
}